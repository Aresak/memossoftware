# Welcome #
Welcome to an app for patients' diagnoses.
This app was made for an interview to use Vue.js.


# Installation #
1. Download the code and put it on your webserver.
2. Parse the [Install/install.sql](https://bitbucket.org/Aresak/memossoftware/src/master/developer/projects/memos/install/tables.sql) SQL query into your database to create required tables.
4. Configure the SQL authentication in the [libs/loader.php](https://bitbucket.org/Aresak/memossoftware/src/master/developer/projects/memos/libs/loader.php).
5. Enjoy!

# Example & Live Demo #
Find the live demo at [http://developer.symbiant.cz/projects/memos/](http://developer.symbiant.cz/projects/memos/).

The testing diagnoses was fetched from Dr. House Series 1.