<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Memos</title>

    <link rel="stylesheet" href="http://developer.symbiant.cz/projects/memos/libs/bootstrap.min.css">
    <link rel="stylesheet" href="index.css">

    <script src="http://developer.symbiant.cz/projects/memos/libs/jquery-3.3.1.slim.min.js"></script>
    <script src="http://developer.symbiant.cz/projects/memos/libs/popper.min.js"></script>
    <script src="http://developer.symbiant.cz/projects/memos/libs/bootstrap.min.js"></script>
  </head>
  <body>
    <div id="app">
      <!-- The list of patients window -->
      <div id="patients_list">
        <table class="table table-striped table-hover">
          <!-- This line shows if there is an ajax processing -->
          <tr v-if="ajaxProcessing">
            <td>
              <i>Loading patients...</i>
            </td>
          </tr>
          <!-- A button to create a new patient -->
          <tr class="newPatientButton" @click="createNew">
            <td></td>
            <td>New patient</td>
            <td></td>
          </tr>
          <!-- A list of patients -->
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
          </tr>
          <tr @click="openPatient(patient.id)" v-for="patient in patients">
            <td>{{patient.id}}</td>
            <td>{{patient.last_name}}</td>
            <td>{{patient.first_name}}</td>
          </tr>
        </table>
        <!-- This disables ability to click any new patient when there is an new patient creating. -->
        <div class="disabled-patients_list" v-if="isCreatingNewPatient()"></div>
      </div>

      <!-- The patient's details window -->
      <div id="patients_details">
        <!-- A block that displays when there is a new patient being created. -->
        <div id="details_new" v-if="isCreatingNewPatient">
          <table class="table">
            <tr>
              <td align="right">
                First Name:
              </td>
              <td align="left">
                <input type="text" v-model="patient.first_name">
              </td>
            </tr>

            <tr>
              <td align="right">
                Last Name:
              </td>
              <td align="left">
                <input type="text" v-model="patient.last_name">
              </td>
            </tr>

            <tr>
              <td align="right">
                Email:
              </td>
              <td align="left">
                <input type="email" v-model="patient.email">
              </td>
            </tr>

            <tr>
              <td align="right">
                Phone:
              </td>
              <td align="left">
                <input type="tel" v-model="patient.phone">
              </td>
            </tr>

            <tr>
              <td align="right">
                Date of birth:
              </td>
              <td align="left">
                <input type="date" v-model="patient.birthday">
              </td>
            </tr>

            <!-- Buttons row -->
            <tr>
              <td align="right">
                <button class="pointer btn btn-primary" @click="saveNewPatient">
                  Save new patient
                </button>
              </td>
              <td align="left">
                <button class="pointer btn" @click="closeNewPatient">
                  Close
                </button>
              </td>
            </tr>
          </table>
        </div>

        <!-- This block shows when there is an patient loaded and we are not creating a new patient. -->
        <div id="details" v-else-if="patient.id != 0">
          <!-- A button to delete the patient -->
          <div class="patient-option">
            <button class="pointer btn btn-danger" @click="deletePat()">
              Delete patient
            </button>
          </div>

          <!-- A table with patient's details -->
          <table class="table">
            <tr @click="edit('first_name')">
              <td align="right" class="n70">
                First Name:
              </td>
              <td align="left">
                {{patient.first_name}}
              </td>
            </tr>

            <tr @click="edit('last_name')">
              <td align="right" class="n70">
                Last Name:
              </td>
              <td align="left">
                {{patient.last_name}}
              </td>
            </tr>

            <tr @click="edit('email')">
              <td align="right" class="n70">
                Email:
              </td>
              <td align="left">
                {{patient.email}}
              </td>
            </tr>

            <tr @click="edit('phone')">
              <td align="right" class="n70">
                Phone:
              </td>
              <td align="left">
                {{patient.phone}}
              </td>
            </tr>

            <tr @click="edit('dob')">
              <td align="right" class="n70">
                Date of birth:
              </td>
              <td align="left">
                {{patient.birthday}}
              </td>
            </tr>

            <tr>
              <td align="right" class="n70">
                ID:
              </td>
              <td align="left">
                {{patient.id}}
              </td>
            </tr>
          </table>
        </div>
        <!-- This shows up when there is an ajax processing on. -->
        <span v-else-if="ajaxProcessing">Loading patient's data... Please wait.</span>

        <!-- This shows when there are none actions to do for this window. -->
        <span v-else>Select your patient first to see his details.</span>

        <!-- This shows the list of patient's diagnoses window if the patient is loaded -->
        <div id="diagnoses" v-if="patient.id != 0">
          <table class="table">
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th class="actions-td">Actions</th>
            </tr>

            <tr v-for="(diagnose, index) in found_diagnoses">
              <td>{{diagnose.name}}</td>
              <td>{{diagnose.description}}</td>
              <td class="actions-td">
                <button class="pointer btn btn-danger" @click="delDiagnose(diagnose.id)">Delete</button>
              </td>
            </tr>
          </table>
        </div>

        <!-- This shows the list of all diagnoses window to add to the patient. -->
        <div id="add-diagnose" v-if="patient.id != 0">
          <input style="width: 100%;" type="search" v-on:keyup="searchDiagnoses" v-model="search_for_diagnose" placeholder="Search for diagnose to add">
          <table class="table">
            <tr class="pointer" v-for="diagnose in diagnoses" @click="addDiagnose(diagnose.id)">
              <td>{{diagnose.name}}</td>
              <td>{{diagnose.description}}</td>
            </tr>
          </table>
        </div>
      </div>

      <!-- This is a window popup to edit patient's detail field. -->
      <div id="patients_edit" v-if="isOpen">
        <table class="table">
          <tr>
            <td align="right"><b>{{pretty_field}}:</b></td>
            <td align="left">
              <input type="email" v-model="value" v-bind:value="value" v-if="field == 'email'">
              <input type="tel" v-model="value" v-bind:value="value" v-else-if="field == 'phone'">
              <input type="date" v-model="value" v-bind:value="value" v-else-if="field == 'dob'">
              <input type="text" v-model="value" v-bind:value="value" v-else>
            </td>
          </tr>
          <!-- Buttons row -->
          <tr>
            <td></td>
            <td>
              <button @click="close" class="btn">Close</button>
              <button @click="save" class="btn btn-primary" :disabled="ajaxProcessing">Save</button>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </body>

  <!-- Load the vue -->
  <script src="libs/vue.min.js"></script>
  <script src="index.js"></script>
</html>
