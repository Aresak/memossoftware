<pre>
<?php
  error_reporting(E_ALL);
  ini_set("display_errors", 1);
  // This is a dummy test file that will do a simple test of my functions

  // Define the libs path and run the loader
  $libs_path = "libs/";
  require_once $libs_path . "loader.real.php";

  function m($x) {
    echo $x . "<br>";
  }

  function i($x) {
    echo "<b>Info:</b> $x<br>";
  }

  function c($x) {
    echo "<b>Call:</b> $x<br>";
  }

  function ok($x) {
    echo "<b>OK:</b> $x<br>";
  }

  i("Loader has started.");
  m("Starting the Patient test...");



  c("Creating a dummy patient...");

  $p_insertNew = Patient::InsertNew($sql, "First", "Last", time(), "my@mail.com", 123456789);

  if($p_insertNew != null) {
    ok("Dummy patient created. Patient::InsertNew()");
  } else {
    m("Dummy patient failed to create.");
    die();
  }

  c("Displaying all getters!");
  m("ID: " . $p_insertNew->id());
  m("First Name: " . $p_insertNew->name_first());
  m("Last Name: " . $p_insertNew->name_last());
  m("Date of Birth: " . $p_insertNew->dob());
  m("Email: " . $p_insertNew->email());
  m("Phone: " . $p_insertNew->phone());
  ok("All getters are ok.");



  c("Loading the patient from his ID (" . $p_insertNew->id() . ")");
  $p_selectID = Patient::CreateFrom_ID($sql, $p_insertNew->id());

  if($p_selectID != null) {
    ok("Dummy patient selected by his ID. Patient::CreateFrom_ID()");
  } else {
    m("Dummy patient failed to select by ID.");
    die();
  }



    c("Loading the patient from his Email (my@mail.com)");
    $p_selectEmail = Patient::CreateFrom_Email($sql, "my@mail.com");

    if($p_selectEmail != null) {
      ok("Dummy patient selected by his Email address. Patient::CreateFrom_Email()");
    } else {
      m("Dummy patient failed to select by email.");
      die();
    }



    c("Loading the patient from his Phone number. (123456789)");
    $p_selectPhone = Patient::CreateFrom_Phone($sql, "123456789");

    if($p_selectPhone != null) {
      ok("Dummy patient selected by his Phone number. Patient::CreateFrom_Phone()");
    } else {
      m("Dummy patient failed to select by phone.");
      die();
    }



    c("Loading the patient from his name. (First Last)");
    $p_selectName = Patient::CreateFrom_Name($sql, "First Last");

    if($p_selectName != null) {
      ok("Dummy patient selected by his full name. Patient::CreateFrom_Name()");
    } else {
      m("Dummy patient failed to select by full name.");
      die();
    }


    c("Loading all patients in the database.");
    $p_allPatients = Patient::GetAll($sql);
    var_dump($p_allPatients);
    foreach($p_allPatients as $patient) {
      ok("Patient #" . $patient->id() . ": " . $patient->name_first() . " " . $patient->name_last());
    }



    c("Testing all setters!");
    $p_insertNew->name_first("First2");
    ok("First name changed: " . $p_insertNew->name_first());

    $p_insertNew->name_last("Last2");
    ok("Last name changed: " . $p_insertNew->name_last());

    $p_insertNew->dob(time());
    ok("Date of birth changed: " . $p_insertNew->dob());

    $p_insertNew->email("com@mail.my");
    ok("Email changed: " . $p_insertNew->email());

    $p_insertNew->phone("987654321");
    ok("Phone changed: " . $p_insertNew->phone());


    c("Deleting the latest dummy patient.");
    $p_insertNew->delete();
    ok("Dummy patient deleted.");



    ok("Patient class is OK!");



    m("Starting the Diagnose test...");

    c("Creating a new diagnose.");
    $d_insertNew = Diagnose::InsertNew($sql, "Mild Headache", "These symptoms include mild head pain that is aching, squeezing, or band-like, on both sides of the head, generally above the level of the eyebrows.");

    ok("New diagnose has been created.");

    c("Displaying all getters!");
    m("ID: " . $d_insertNew->id());
    m("Name: " . $d_insertNew->name());
    m("Description: " . $d_insertNew->description());
    ok("All getters display correctly.");

    c("Loading diagnose from the ID. (" . $d_insertNew->id() . ")");
    $d_selectID = Diagnose::CreateFrom_ID($sql, $d_insertNew->id());

    if($d_selectID != null) {
      ok("Dummy diagnose selected by it's ID. Diagnose::CreateFrom_ID()");
    } else {
      m("Dummy diagnose failed to select by it's ID.");
      die();
    }

    c("Loading diagnose by a name keyword 'Headache'.");
    $d_selectName = Diagnose::CreateFrom_SimiliarName($sql, "Headache");

    if(count($d_selectName) > 0) {
      ok("Dummy diagnose selected by a name keyword 'Headache'");
    } else {
      m("Dummy diagnose failed to select by a name keyword 'Headache'");
      die();
    }

    c("Loading diagnose by a description keywords 'mild head pain'");
    $d_selectDescription = Diagnose::CreateFrom_SimiliarDescription($sql, "mild head pain");

    if(count($d_selectDescription) > 0) {
      ok("Dummy diagnose selected by a description keywords 'mild head pain'");
    } else {
      m("Dummy diagnose failed to select by a description keywords 'mild head pain'");
      die();
    }

    c("Loading all diagnoses.");
    $d_all = Diagnose::GetAll($sql);

    if(count($d_all) > 0) {
      foreach($d_all as $diagnose) {
        ok("Diagnose #" . $diagnose->id() . ": " . $diagnose->name() . " - " . $diagnose->description());
      }
    } else {
      m("Dummy diagnoses failed to load by a getall function.");
      die();
    }


    c("Testing all setters!");
    $d_insertNew->name("Osteogenesis Imperfecta");
    ok("Name changed: " . $d_insertNew->name());
    $d_insertNew->description("All people with osteogenesis imperfecta have brittle bones. OI can range from mild to severe and symptoms vary from person to person.");
    ok("Description changed: " . $d_insertNew->description());


    c("Deleting dummy diagnose.");
    $d_insertNew->delete();
    ok("Dummy diagnose has been deleted.");


    ok("Diagnose class is OK!");


    m("Starting the Found_Diagnose Test...");

    c("Creating a dummy Found Diagnose (Pat: 20, Diagnose: 30)");
    $fd_insertNew = Found_Diagnose::InsertNew($sql, 20, 30);

    ok("Dummy Found Diagnose has been created.");

    c("Displaying all getters!");
    m("ID: " . $fd_insertNew->id());
    m("Patient ID: " . $fd_insertNew->patient_id());
    m("Diagnose ID: " . $fd_insertNew->diagnose_id());
    ok("Getters are displayed OK.");

    c("Loading Found Diagnose by ID");

    $fd_selectID = Found_Diagnose::CreateFrom_ID($sql, $fd_insertNew->id());
    if($fd_selectID != null) {
      ok("Found Diagnose has been loaded with ID. Found_Diagnose::CreateFrom_ID()");
    } else {
      m("Found Diagnose failed to load with ID.");
      die();
    }

    c("Loading Found Diagnose by Patient's ID.");
    $fd_selectPatient = Found_Diagnose::CreateFrom_PatientID($sql, 20);

    if(count($fd_selectPatient) > 0) {
      foreach($fd_selectPatient as $fd) {
        ok("Found Diagnose #" . $fd->id() . ": For patient " . $fd->patient_id() . " having diagnose " . $fd->diagnose_id());
      }
    } else {
      m("Failed to load Found Diagnoses by Patient's ID.");
      die();
    }


    c("Loading Found Diagnose by Diagnose's ID.");
    $fd_selectDiagnose = Found_Diagnose::CreateFrom_DiagnoseID($sql, 30);

    if(count($fd_selectDiagnose) > 0) {
      foreach($fd_selectDiagnose as $fd) {
        ok("Found Diagnose #" . $fd->id() . ": For patient " . $fd->patient_id() . " having diagnose " . $fd->diagnose_id());
      }
    } else {
      m("Failed to load Found Diagnoses by Diagnose's ID.");
      die();
    }


    c("Loading all Found Diagnoses.");
    $fd_all = Found_Diagnose::GetAll($sql);

    if(count($fd_all) > 0) {
      foreach($fd_all as $fd) {
        ok("Found Diagnose #" . $fd->id() . ": For patient " . $fd->patient_id() . " having diagnose " . $fd->diagnose_id());
      }
    } else {
      m("Failed to load all Found Diagnoses.");
      die();
    }


    c("Testing all setters.");
    $fd_insertNew->patient_id(21);
    ok("Patient's ID changed: " . $fd_insertNew->patient_id());
    $fd_insertNew->diagnose_id(31);
    ok("Diagnose's ID changed: " . $fd_insertNew->diagnose_id());


    c("Deleting dummy found diagnose.");
    $fd_insertNew->delete();
    ok("Dummy Found Diagnose is deleted.");

    ok("Found Diagnose class Test is OK!");


    ok("All tests are OK!");
?>
</pre>
