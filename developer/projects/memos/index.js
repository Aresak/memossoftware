/**
 *  This is a sandbox object so you can create new diagnoses to choose from.
 *
 */
var sandbox = {
  /**
   *  Method to add a new diagnose to the database.
   *
   */
  addDiagnose: function(name, description) {
    fetch("post.php?sandbox_addDiagnose&name=" + name + "&description=" + description)
    .then(function(response) {
      console.log("Added diagnose " + name);
    })
  }
}


/**
 *  This is an object which works with the list of patients.
 *
 */
var list = new Vue({
  el: "#patients_list",
  data: {
    patients: [],
    ajaxProcessing: false,
  },
  methods: {
    // Open a new patient in the details window
    openPatient: function(pid) {
      console.log("Openning patient's details id: " + pid);
      details.display(pid);
    },
    // Craete a new patient in the details window
    createNew: function() {
      console.log("Creating new patient");
      details.close();
      details.isCreatingNewPatient = true;
    },
    // Function to check whether an user is creating a new patient
    isCreatingNewPatient: function() {
      /** This function was urgent since the page didn't load the details at first and was throwing an
          "details is undefined" error. */
      if(details == null)
        return false;

      return details.isCreatingNewPatient;
    },
    // Method which reloads the list of patients
    reload: function() {
      this.ajaxProcessing = true;
      var vm = this;

      fetch("post.php?patients_list")
      .then(function(response) {
        return response.json();
      })
      .then(function(data) {
        vm.ajaxProcessing = false;
        vm.patients = data;
      })
    }
  },
  // Method to load the list of patients
  created: function() {
    this.reload();
  }
});


/**
 *  Objects which works with the details window.
 *
 **/
var details = new Vue({
  el: "#patients_details",
  data: {
    search_for_diagnose: "",
    isCreatingNewPatient: false,
    patient: {
      id: 0,
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      birthday: 0,
    },
    found_diagnoses: {

    },
    diagnoses: {

    },
    ajaxProcessing: false
  },
  methods: {
    // Reload the found diagnoses window
    reloadFoundDiagnoses: function() {
      this.ajaxProcessing = true;
      var vm = this;

      fetch("post.php?diagnoses=" + this.patient.id)
      .then(function(response) {
        return response.json();
      })
      .then(function(data_fd) {
        vm.found_diagnoses = data_fd;
        console.log("Received discovered diagnoses for this patient.")
      })
    },
    // Display the patient's details and load the found diagnoses.
    display: function(pid) {
      this.ajaxProcessing = true;
      var vm = this;

      // Loads the patient's diagnoses first
      fetch("post.php?diagnoses=" + pid)
      .then(function(response) {
        return response.json();
      })
      .then(function(data_fd) {
        vm.found_diagnoses = data_fd;
        console.log("Received discovered diagnoses for this patient.")

        // Then the patient's personal data
        fetch("post.php?patient=" + pid)
        .then(function(response) {
          return response.json();
        })
        .then(function(data_p) {
          vm.ajaxProcessing = false;
          vm.patient = data_p;

          console.log("Received patient's personal data.");

          vm.searchDiagnoses();
        })
      })
    },
    // Method loads the similiar diagnoses to an input field
    searchDiagnoses: function() {
      console.log("Search Diagnoses");
      var vm = this;
      this.ajaxProcessing = true;


      fetch("post.php?diagnoses_all=" + this.search_for_diagnose)
      .then(function(response) {
        return response.json();
      })
      .then(function(data_d) {
        vm.ajaxProcessing = false;
        vm.diagnoses = data_d;

        console.log("Received similiar diagnoses.");
      })
    },
    // Delete the found diagnose from the patient
    delDiagnose: function(fdid) {
      if(!confirm("Are you sure to delete the discovered diagnose?"))
        return;

      this.ajaxProcessing = true;
      var vm = this;

      fetch("post.php?del_diagnose=" + fdid)
      .then(function(response) {
        vm.ajaxProcessing = false;

        console.log("Deleted old diagnose.");

        vm.reloadFoundDiagnoses();
      })
    },
    // Add a new found diagnose to the patient
    addDiagnose: function(did) {
      this.ajaxProcessing = true;
      var vm = this;

      fetch("post.php?add_diagnose=" + did + "&pid=" + this.patient.id)
      .then(function(response) {
        vm.ajaxProcessing = false;

        console.log("Added new discovered diagnose.");

        vm.reloadFoundDiagnoses();
      })
    },
    // Deletes an existing patient
    deletePat: function() {
      if(!confirm("Are you sure to delete this patient?"))
        return;

      ajaxProcessing = true;
      var vm = this;

      fetch("post.php?delpatient=" + this.patient.id)
      .then(function(response) {
        vm.ajaxProcessing = false;
        console.log("Patient " + vm.patient.first_name + " " + vm.patient.last_name + " deleted.");
        vm.close();
        list.reload();
      })
    },
    // Opens an edit popup window with the field update info
    edit: function(field) {
      var v;
      switch(field) {
        case "first_name":
          v = this.patient.first_name;
          break;
        case "last_name":
          v = this.patient.last_name;
          break;
        case "email":
          v = this.patient.email;
          break;
        case "phone":
          v = this.patient.phone;
          break;
        case "dob":
          v = this.patient.birthday;
          break;
      }
      edit.open(field, v);
    },
    // Closes the details window
    close: function() {
      this.patient.id = 0;
      this.patient.first_name = "";
      this.patient.last_name = "";
      this.patient.email = "";
      this.patient.phone = "";
      this.patient.birthday = "";
    },
    // Saves and creates a new patient
    saveNewPatient: function() {
      this.ajaxProcessing = true;
      var vm = this;

      fetch("post.php?addPatient=" + JSON.stringify(this.patient))
      .then(function(response) {
        vm.ajaxProcessing = false;
        console.log("New patient created.");
        vm.closeNewPatient();
        list.reload();
      })
    },
    // Closes the create new patient block and details window.
    closeNewPatient: function() {
      this.isCreatingNewPatient = false;
      this.close();
    }
  }
});



/**
 *  Object which works with the edit popup window
 *
 **/
var edit = new Vue({
  el: "#patients_edit",
  data: {
    field: "",
    pretty_field: "",
    value: "",
    isOpen: false,
    ajaxProcessing: false
  },
  methods: {
    // Saves the field with new data
    save: function() {
      var vm = this;
      this.ajaxProcessing = true;

      fetch("post.php?edit=" + details.patient.id + "&f=" + this.field + "&v=" + this.value)
      .then(function(response) {
        vm.ajaxProcessing = false;
        console.log("Field " + vm.field + " has been set to " + vm.value);

        switch(vm.field) {
          case "first_name":
            details.patient.first_name = vm.value;
            break;
          case "last_name":
            details.patient.last_name = vm.value;
            break;
          case "email":
            details.patient.email = vm.value;
            break;
          case "phone":
            details.patient.phone = vm.value;
            break;
          case "dob":
            details.patient.birthday = vm.value;
            break;
        }

        vm.close();
      })
    },
    // Closes the edit popup window
    close: function() {
      this.field = "";
      this.value = "";
      this.isOpen = false;
      this.pretty_field = "";
    },
    // Opens the edit popup window
    open: function(field, value) {
      this.field = field;
      this.value = value;
      this.isOpen = true;

      switch(field) {
        case "first_name":
          this.pretty_field = "First Name";
          break;
        case "last_name":
          this.pretty_field = "Last Name";
          break;
        case "email":
          this.pretty_field = "Email";
          break;
        case "phone":
          this.pretty_field = "Phone";
          break;
        case "dob":
          this.pretty_field = "Birthday";
          break;
      }
    }
  }
})
