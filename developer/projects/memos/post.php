<?php
// Load the required libraries
$libs_path = "libs/";
require_once $libs_path . "loader.real.php";


// Check if the request is to return list of patients
if(isset($_GET["patients_list"])) {
  // The request is to send the list of patients in json format
  $patients                 = array();
  foreach(Patient::GetAll($sql) as $pat) {
    $patient                = array();
    $patient["id"]          = $pat->id();
    $patient["first_name"]  = $pat->name_first();
    $patient["last_name"]   = $pat->name_last();

    array_push($patients, $patient);
  }

  echo json_encode($patients);
}


// Check if the request is to return patient's personal data
else if(isset($_GET["patient"])) {
  // The request is to send patient's personal data.
  $patient            = $_GET["patient"];

  $patient            = Patient::CreateFrom_ID($sql, $patient);

  $out                = array();

  if($patient == null) {
    $out["error"] = "Patient with this ID doesn't exist.";
    die(json_encode($out));
  }

  $out["id"]          = $patient->id();
  $out["first_name"]  = $patient->name_first();
  $out["last_name"]   = $patient->name_last();
  $out["email"]       = $patient->email();
  $out["phone"]       = $patient->phone();
  $out["birthday"]    = $patient->dob();

  echo json_encode($out);
}



// Check if the request is to return patient's discovered diagnoses
else if(isset($_GET["diagnoses"])) {
  // The request is to send diagnoses matching to the patient.
  $out                                    = array();

  foreach(Found_Diagnose::CreateFrom_PatientID($sql, $_GET["diagnoses"]) as $diagnose) {
    $out[$diagnose->id()]["id"]           = $diagnose->id();
    $out[$diagnose->id()]["did"]          = $diagnose->diagnose_id();
    $out[$diagnose->id()]["pid"]          = $diagnose->patient_id();

    $diagnose_data                        = Diagnose::CreateFrom_ID($sql, $diagnose->diagnose_id());
    $out[$diagnose->id()]["name"]         = $diagnose_data->name();
    $out[$diagnose->id()]["description"]  = $diagnose_data->description();
  }

  echo json_encode($out);
}


// Check if the request is to return a list of similiar diagnoses
else if(isset($_GET["diagnoses_all"])) {
  // The request is to send all possible diagnoses.
  $out                                    = array();

  if(empty($_GET["diagnoses_all"])) {
    $diagnoses = Diagnose::GetAll($sql);
  } else {
    $diagnoses = Diagnose::CreateFrom_SimiliarName($sql, $_GET["diagnoses_all"]);
  }


  foreach($diagnoses as $diagnose) {
    $out[$diagnose->id()]["id"]           = $diagnose->id();
    $out[$diagnose->id()]["name"]         = $diagnose->name();
    $out[$diagnose->id()]["description"]  = $diagnose->description();

  }

  echo json_encode($out);
}


// Check if the request is to delete an discovered diagnose from the patient
else if(isset($_GET["del_diagnose"])) {
  // The request is to delete discovered diagnose from the patient
  $diagnose = Found_Diagnose::CreateFrom_ID($sql, $_GET["del_diagnose"]);

  if($diagnose != null) {
    $diagnose->delete();
  } else {
    echo "Failed to delete diagnose - No matching ID.";
  }
}


// Check if the request is to add a new discovered diagnose to the patient
else if(isset($_GET["add_diagnose"])) {
  // The request is to add a new discovered diagnose to the patient
  if(!isset($_GET["pid"])) {
    die("Need patient's ID to add a new discovered diagnose to him.");
  }

  Found_Diagnose::InsertNew($sql, $_GET["pid"], $_GET["add_diagnose"]);
}


// Check if the request is to edit patient's detail field
else if(isset($_GET["edit"])) {
  // The request is to edit patient's detail field
  if(!isset($_GET["f"])) {
    die("Need field property to update.");
  }

  if(!isset($_GET["v"])) {
    die("Need value property to update.");
  }

  $patient = Patient::CreateFrom_ID($sql, $_GET["edit"]);

  if($patient == null) {
    die("Patient not found by this ID.");
  }

  // Update the detail field
  switch($_GET["f"]) {
    case "first_name":
      $patient->name_first($_GET["v"]);
      break;
    case "last_name":
      $patient->name_last($_GET["v"]);
      break;
    case "email":
      $patient->email($_GET["v"]);
      break;
    case "phone":
      $patient->phone($_GET["v"]);
      break;
    case "dob":
      $patient->dob($_GET["v"]);
      break;
    default:
      die("Cannot change this field!");
      break;
  }
}


// Check if the request is to add a new patient
else if(isset($_GET["addPatient"])) {
  // The request is to add a new patient
  $data = json_decode($_GET["addPatient"], true);

  Patient::InsertNew($sql, $data["first_name"], $data["last_name"], $data["birthday"], $data["email"], $data["phone"]);
}


// Check if the request is to delete an existing patient
else if(isset($_GET["delpatient"])) {
  // The request is to delete an existing patient
  $patient = Patient::CreateFrom_ID($sql, $_GET["delpatient"]);

  if($patient == null) {
    die("Error: Patient not found by this ID.");
  }

  foreach(Found_Diagnose::CreateFrom_PatientID($sql, $patient->id()) as $fd) {
    $fd->delete();
  }

  $patient->delete();
}


// Check if the request is to add a new possible diagnose
else if(isset($_GET["sandbox_addDiagnose"])) {
  // The request is to add a new possible diagnose
  if(!isset($_GET["name"]))
    die("Error: Name is required parameter");

  if(!isset($_GET["description"]))
    die("Error: Description is required parameter");

  Diagnose::InsertNew($sql, $_GET["name"], $_GET["description"]);
}
?>
