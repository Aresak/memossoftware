CREATE TABLE IF NOT EXISTS `memos_patients` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name_first` text NOT NULL,
  `name_last` text NOT NULL,
  `dob` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains patient''s general data' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `memos_found_diagnoses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `diagnose_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains data of found diagnoses attached to the patient' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `memos_diagnoses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains list of possible diagnoses' AUTO_INCREMENT=1 ;
