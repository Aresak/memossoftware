<?php
/**
  * Class which holds and manipulates with the Found Diagnoses table.
  *
  *  @author Tomas Malcanek
  *  @version 1.0
  *  @see Patient           The patient manipulation class
  *  @see Diagnose          The diagnose manipulation class
  */
class Found_Diagnose {
  /** @var int The ID of the found diagnose in the table. */
  private $ID;

  /** @var int The ID of the patient which links to this discovery. */
  private $patient_id;

  /** @var int The ID of the diagnose which links to this discovery. */
  private $diagnose_id;


  /**  @var mysqli The MySQLi connection to the database which contains the table with found diagnoses. */
  private $sql;



  /**
   *  Get an diagnose discovery object from the ID in the table.
   *
   *  @param mysqli $sql  The MySQLi connection to the database which contains the table with found diagnoses.
   *  @param int    $id   The ID of the discovered diagnose.
   *
   *  @since 1.0
   *
   *  @return Found_Diagnose|null Returns the Found Diagnose class when the ID matches, otherwise returns an null.
   */
  public static function CreateFrom_ID($sql, $id) {
    // Prepare the ID to go into the database
    $id         = mysqli_escape_string($sql, $id);

    // Query the SQL
    $idQuery    = "SELECT * FROM memos_found_diagnoses WHERE ID='$id'";
    $idResult   = mysqli_query($sql, $idQuery)
                    or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($idResult) == 0) {
      return null;
    }

    $f_diagnose = new Found_Diagnose($sql);
    $f_diagnose->initializeFromResource($idResult);

    return $f_diagnose;
  }

  /**
   *  Get diagnose discoveries for the given patient ID.
   *
   *  @param mysqli $sql The MySQLi connection to the database which contains the table with found diagnoses.
   *  @param int    $id  The ID of the patient
   *
   *  @since 1.0
   *
   *  @return Found_Diagnose[] Returns an array with found diagnoses for given patient ID.
   */
  public static function CreateFrom_PatientID($sql, $id) {
    // Prepare the ID to go into the database
    $id         = mysqli_escape_string($sql, $id);

    // Query the SQL
    $idQuery    = "SELECT * FROM memos_found_diagnoses WHERE patient_id='$id'";
    $idResult   = mysqli_query($sql, $idQuery)
                    or die(mysqli_error($sql));

    // Get the patients into the object array
    $f_diagnoses = array();
    for($index = 0; $index < mysqli_num_rows($idResult); $index ++) {
      $f_diagnose = new Found_Diagnose($sql);
      $f_diagnose->initializeFromResource($idResult, $index);

      array_push($f_diagnoses, $f_diagnose);
    }

    return $f_diagnoses;
  }

  /**
   *  Get diagnose discoveries for the given diagnose ID.
   *
   *  @param mysqli $sql  The MySQLi connection to the database which contains the table with found diagnoses.
   *  @param int    $id   The ID of the diagnose.
   *
   *  @since 1.0
   *
   *  @return Found_Diagnose[] Returns an array with found diagnoses for the given diagnose ID.
   */
  public static function CreateFrom_DiagnoseID($sql, $id) {
    // Prepare the ID to go into the database
    $id             = mysqli_escape_string($sql, $id);

    // Query the SQL
    $idQuery        = "SELECT * FROM memos_found_diagnoses WHERE diagnose_id='$id'";
    $idResult       = mysqli_query($sql, $idQuery)
                        or die(mysqli_error($sql));

    // Get the diagnoses into the object array
    $f_diagnoses = array();
    for($index = 0; $index < mysqli_num_rows($idResult); $index ++) {
      $f_diagnose   = new Found_Diagnose($sql);
      $f_diagnose->initializeFromResource($idResult, $index);

      array_push($f_diagnoses, $f_diagnose);
    }

    return $f_diagnoses;
  }

  /**
   *  Get all diagnose discoveries in the table.
   *
   *  @param mysqli $sql  The MySQLi connection to the database which contains the table with found diagnoses.
   *
   *  @since 1.0
   *
   *  @return Found_Diagnose[] Returns an array with all found diagnoses
   */
  public static function GetAll($sql) {
    // Query the SQL
    $idQuery        = "SELECT * FROM memos_found_diagnoses";
    $idResult       = mysqli_query($sql, $idQuery)
                        or die(mysqli_error($sql));

    // Get all rows into the object array
    $f_diagnoses = array();
    for($index = 0; $index < mysqli_num_rows($idResult); $index ++) {
      $f_diagnose   = new Found_Diagnose($sql);
      $f_diagnose->initializeFromResource($idResult, $index);

      array_push($f_diagnoses, $f_diagnose);
    }

    return $f_diagnoses;
  }

  /**
   *  Creates a new diagnose discovery.
   *
   *  @param mysqli $sql            The MySQLi connection to the database which contains the table with found diagnoses.
   *  @param int    $patient_id     The ID of the patient
   *  @param int    $diagnose_id    The ID of the diagnose
   *
   *  @since 1.0
   *
   *  @return Found_Diagnose  Returns the newly created diagnose discovery.
   */
  public static function InsertNew($sql, $patient_id, $diagnose_id) {
    // Prepare the values to go into the database
    $patient_id   = mysqli_escape_string($sql, $patient_id);
    $diagnose_id  = mysqli_escape_string($sql, $diagnose_id);

    // Query the SQL
    $insertQuery  = "INSERT INTO memos_found_diagnoses (patient_id, diagnose_id) VALUES ('$patient_id', '$diagnose_id')";
    $insertResult = mysqli_query($sql, $insertQuery)
                      or die(mysqli_error($sql));

    // Return the last added discovery
    return Found_Diagnose::CreateFrom_ID($sql, mysqli_insert_id($sql));
  }


  /**
   *  Prepares the object with MySQLi connection.
   *
   *  @param mysqli $sql      The MySQLi connection to the database which contains the table with found diagnoses.
   *
   *  @since 1.0
   **/
  public function __construct($sql) {
    $this->sql = $sql;
  }

  /**
   *  Initializes the Found Diagnose object from the given resource at row index.
   *
   *  @param mysqli_result  $resource   The MySQLi result resource, which contains the found diagnose data.
   *  @param int            $index      The row index of the resource
   *
   *  @since 1.0
   **/
  public function initializeFromResource($resource, $index = 0) {
    // Set the properties from the values in the database
    $this->ID           = mysqli_result($resource, $index, "ID");
    $this->patient_id   = mysqli_result($resource, $index, "patient_id");
    $this->diagnose_id  = mysqli_result($resource, $index, "diagnose_id");
  }

  /**
   *  Deletes the found diagnose from the table.
   *
   *  @since 1.0
   */
  public function delete() {
    // Query the SQL
    $deleteQuery  = "DELETE FROM memos_found_diagnoses WHERE ID='" . $this->ID . "'";
    $deleteResult = mysqli_query($this->sql, $deleteQuery)
                      or die(mysqli_error($this->sql));
  }

  /**
   *  Returns the ID of the found diagnose in the table.
   *
   *  @since 1.0
   *
   *  @return int The ID of the found diagnose in the table.
   */
  public function id() {
    return $this->ID;
  }

  /**
   *  Get or set the patient's ID.
   *
   *  @param int $newValue  The new patient's ID.
   *
   *  @since 1.0
   *
   *  @return int|null  Returns an patient's ID when the new value is null,
   *                    or returns a null when the new value is set.
   */
  public function patient_id($newValue = null) {
    if($newValue == null) {
      // The request was to get the patient's ID
      return $this->patient_id;
    }

    // The request was to set the patient's ID in the database.
    // Prepare the value to go into the database
    $newValue     = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL...
    $updateQuery  = "UPDATE memos_found_diagnoses SET patient_id='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult = mysqli_query($this->sql, $updateQuery)
                      or die(mysqli_error($this->sql));

    // Update the value in this object
    $this->patient_id = $newValue;
    return null;
  }

  /**
   *  Get or set the diagnose's ID.
   *
   *  @param int $newValue  The new diagnose's ID.
   *
   *  @since 1.0
   *
   *  @return int|null  Returns an diagnose's ID when the new value is null,
   *                    or returns a null when the new value is set.
   */
  public function diagnose_id($newValue = null) {
    if($newValue == null) {
      // The request was to get the diagnose's ID
      return $this->diagnose_id;
    }

    // The request was to set the diagnose's ID in the database.
    // Prepare the value to go into the database
    $newValue     = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL...
    $updateQuery  = "UPDATE memos_found_diagnoses SET diagnose_id='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult = mysqli_query($this->sql, $updateQuery)
                      or die(mysqli_error($this->sql));

    // Update the value in this object
    $this->diagnose_id = $newValue;
    return null;
  }
}

?>
