<?php
/**
 *  Class which holds and manipulates data in the Diagnoses table.
 *  @author Tomas Malcanek
 *  @version 1.0
 *  @see Patient          The patient manipulation class
 *  @see Found_Diagnoses  The found diagnoses manipulation class
 */
class Diagnose {
  /** @var int    Has the ID of row in the table. */
  private $ID;

  /** @var string Has the name of the diagnose. */
  private $name;

  /** @var string Has the description of the diagnose. */
  private $description;


  /** @var mysqli The MySQLi connection to the diagnose table. */
  private $sql;



  /**
   *  Creates the Diagnose object from the given ID.
   *
   *  @param mysqli $sql      The MySQLi connection to the database with Diagnose details.
   *  @param int $id      The ID of the diagnose in the table.
   *
   *  @since 1.0
   *  @return Diagnose|null Returns the Diagnose object or null when there is no matching ID.
   *
   *  @see Diagnose::CreateFrom_SimiliarName()          Create the object from similiar name.
   *  @see Diagnose::CreateFrom_SimiliarDescription()   Create the object from similiar description.
   *  @see Diagnose::GetAll()                           Get all objects from the table
   */
  public static function CreateFrom_ID($sql, $id) {
    // Prepare the ID to go into the database
    $id         = mysqli_escape_string($sql, $id);

    // Query the SQL
    $idQuery    = "SELECT * FROM memos_diagnoses WHERE ID='$id'";
    $idResult   = mysqli_query($sql, $idQuery)
                    or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($idResult) == 0) {
      return null;
    }

    $diagnose   = new Diagnose($sql);
    $diagnose->initializeFromResource($idResult);

    return $diagnose;
  }

  /**
   *  Finds and creates the diagnose objects from given similiar name.
   *
   *  @param mysqli $sql      The MySQLi connection to the database with Diagnose details.
   *  @param string $name      The name of the diagnose in the table.
   *
   *  @since 1.0
   *  @return Diagnose[]|null Returns an array of similiar diagnoses.
   *
   *  @see Diagnose::CreateFrom_ID()                    Create the object from given ID.
   *  @see Diagnose::CreateFrom_SimiliarDescription()   Create the object from similiar description.
   *  @see Diagnose::GetAll()                           Get all objects from the table
   */
  public static function CreateFrom_SimiliarName($sql, $name) {
    // Prepare the name to go into the database
    $name         = mysqli_escape_string($sql, $name);

    // Query the SQL
    $nameQuery    = "SELECT * FROM memos_diagnoses WHERE name LIKE '%$name%'";
    $nameResult   = mysqli_query($sql, $nameQuery)
      or die(mysqli_error($sql));

    // Process the results
    $diagnoses    = array();
    for($index = 0; $index < mysqli_num_rows($nameResult); $index ++) {
      $diagnose   = new Diagnose($sql);
      $diagnose->initializeFromResource($nameResult, $index);

      array_push($diagnoses, $diagnose);
    }

    return $diagnoses;
  }

  /**
   *  Finds and creates the diagnose objects from given similiar description.
   *
   *  @param mysqli $sql      The MySQLi connection to the database with Diagnose details.
   *  @param string $description      The details of the diagnose in the table.
   *
   *  @since 1.0
   *  @return Diagnose[]|null Returns an array of similiar diagnoses.
   *
   *  @see Diagnose::CreateFrom_ID()                    Create the object from given ID.
   *  @see Diagnose::CreateFrom_SimiliarName()          Create the object from similiar name.
   *  @see Diagnose::GetAll()                           Get all objects from the table
   */
  public static function CreateFrom_SimiliarDescription($sql, $description) {
    // Prepare the name to go into the database
    $description               = mysqli_escape_string($sql, $description);

    // Query the SQL
    $descriptionQuery   = "SELECT * FROM memos_diagnoses WHERE description LIKE '%$description%'";
    $descriptionResult  = mysqli_query($sql, $descriptionQuery)
                            or die(mysqli_error($sql));

    // Process the results
    $diagnoses          = array();
    for($index = 0; $index < mysqli_num_rows($descriptionResult); $index ++) {
      $diagnose         = new Diagnose($sql);
      $diagnose->initializeFromResource($descriptionResult, $index);

      array_push($diagnoses, $diagnose);
    }

    return $diagnoses;
  }


  /**
   *  Gets all diagnoses in the table.
   *
   *  @param mysqli $sql      The MySQLi connection to the database with Diagnose details.
   *
   *  @since 1.0
   *  @return Diagnose[]|null Returns an array of similiar diagnoses.
   *
   *  @see Diagnose::CreateFrom_ID()                    Create the object from given ID.
   *  @see Diagnose::CreateFrom_SimiliarName()          Create the object from similiar name.
   *  @see Diagnose::GetAll()                           Get all objects from the table
   */
  public static function GetAll($sql) {
    // Query the SQL
    $allQuery = "SELECT * FROM memos_diagnoses";
    $allResult = mysqli_query($sql, $allQuery)
      or die(mysqli_error($sql));

    // Process the results
    $diagnoses = array();
    for($index = 0; $index < mysqli_num_rows($allResult); $index ++) {
      $diagnose = new Diagnose($sql);
      $diagnose->initializeFromResource($allResult, $index);

      array_push($diagnoses, $diagnose);
    }

    return $diagnoses;
  }

  /**
   *  Insert a new diagnose into the table.
   *
   *  @param mysqli $sql          The MySQLi connection to the table with Diagnose details.
   *  @param string $name         The name of the diagnose.
   *  @param string $description  The description of the diagnose.
   *
   *  @since 1.0
   *  @return Diagnose Returns the newly created Diagnose object.
   *
   */
  public static function InsertNew($sql, $name, $description) {
    // Prepare the diagnose values to go into the database
    $name = mysqli_escape_string($sql, $name);
    $description = mysqli_escape_string($sql, $description);

    // Query the SQL
    $insertQuery = "INSERT INTO memos_diagnoses (name, description) VALUES
                  ('$name', '$description')";
    $insertResult = mysqli_query($sql, $insertQuery)
      or die(mysqli_error($sql));

    // Return the last added Diagnose
    return Diagnose::CreateFrom_SimiliarName($sql, $name)[0];
  }


  /**
   *  Prepares the object with MySQLi connection
   *
   *  @param mysqli $sql    The MySQLi connection to the database with Diagnose details
   */
  public function __construct($sql) {
    $this->sql = $sql;
  }

  /**
    * Initializes the Diagnose class from given resource and index.
    *
    * @param mysqli_result $resource    The MySQLi query resource containing the data.
    * @param int           $index       The index of the row in the resource.
    *
    * @since 1.0
    * @see mysqli_result  Used function to easily one-line get the field from the resource.
    */
  public function initializeFromResource($resource, $index = 0) {
    // Set the properties from the values in the database
    $this->ID           = mysqli_result($resource, $index, "ID");
    $this->name         = mysqli_result($resource, $index, "name");
    $this->description  = mysqli_result($resource, $index, "description");
  }

  /**
    * Delete the Diagnose from the table.
    *
    * @since 1.0
    */
  public function delete() {
    // Query the SQL
    $deleteQuery = "DELETE FROM memos_diagnoses WHERE ID='" . $this->ID . "'";
    $deleteResult = mysqli_query($this->sql, $deleteQuery)
      or die(mysqli_error($this->sql));
  }

  /**
   *  Returns the ID of the diagnose in the table.
   *
   *  @since 1.0
   */
  public function id() {
    return $this->ID;
  }

  /**
   *  Get diagnose name or set a new value in the table.
   *
   *  @param string $newValue   The new name of the diagnose.
   *
   *  @since 1.0
   *
   *  @return string|null       Returns the diagnose name if the new value was null,
   *                            or return null if the request was to set the value.
   */
  public function name($newValue = null) {
    if($newValue == null) {
      // The request was to get the name
      return $this->name;
    }

    // The request was to set the name
    // Prepare the new name to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_diagnoses SET name='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->name         = $newValue;
    return null;
  }

  /**
   *  Get diagnose description or set a new value in the table.
   *
   *  @param string $newValue   The new description of the diagnose.
   *
   *  @since 1.0
   *
   *  @return string|null       Returns the diagnose description if the new value was null,
   *                            or return null if the request was to set the value.
   */
  public function description($newValue = null) {
    if($newValue == null) {
      // The request was to get the description
      return $this->description;
    }

    // The request was to set the description
    // Prepare the new description to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_diagnoses SET description='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->description  = $newValue;
    return null;
  }
}
?>
