<?php
/**
 *  Class which holds and manipulates data in the Patients table.
 *  @author Tomas Malcanek
 *  @version 1.0
 *  @see Diagnose         The diagnose class
 *  @see Found_Diagnoses  The found diagnoses class
 */
class Patient {
  /** @var int      Has the ID of the row in the table. */
  private $ID;

  /** @var string   Has the first name of the patient. */
  private $name_first;

  /** @var string   Has the last name of the patient. */
  private $name_last;

  /** @var int      Has the unix time of the patient's date of birth. */
  private $dob;

  /** @var string   Has the patient's email. */
  private $email;

  /** @var int      Has the patient's phone number. */
  private $phone;


  /** @var mysqli   Has the MySQLi connection to the database which holds the patient's data. */
  private $sql;


  /**
   *  Creates the object from the patient's ID in the database.
   *
   *  @param mysqli $sql  The MySQLi connection to the database which holds the patient's data.
   *  @param int    $id   The patient's ID in the database.
   *
   *  @since 1.0
   *  @return Patient|null  The created patient's object, or null if the ID is not found.
   *
   *  @see Patient::CreateFrom_Email()    To create the patient from the email address.
   *  @see Patient::CreateFrom_Phone()    To create the patient from the phone number.
   *  @see Patient::CreateFrom_Name()     To create the patient from his full name.
   *  @see Patient::GetAll()              To get all patients in the database.
  */
  public static function CreateFrom_ID($sql, $id) {
    // Prepare the ID to go into the database
    $id       = mysqli_escape_string($sql, $id);


    // Query the SQL
    $idQuery  = "SELECT * FROM memos_patients WHERE ID='$id'";
    $idResult = mysqli_query($sql, $idQuery)
                  or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($idResult) == 0) {
      return null;
    }

    $patient  = new Patient($sql);
    $patient->initializeFromResource($idResult);

    return $patient;
  }

  /**
    *  Creates the object from the patient's email address in the database.
    *
    *  @param mysqli $sql  The MySQLi connection to the database which holds the patient's data.
    *  @param string $email   The patient's email address.
    *
    *  @since 1.0
    *  @return Patient|null  The created patient's object, or null if the email address is not found.
    *
    *  @see Patient::CreateFrom_ID()       To create the patient from his ID in the database.
    *  @see Patient::CreateFrom_Phone()    To create the patient from the phone number.
    *  @see Patient::CreateFrom_Name()     To create the patient from his full name.
    *  @see Patient::GetAll()              To get all patients in the database.
    **/
  public static function CreateFrom_Email($sql, $email) {
    // Prepare the email to go into the daatabase
    $email        = mysqli_escape_string($sql, $email);

    // Query the SQL
    $emailQuery   = "SELECT * FROM memos_patients WHERE email='$email'";
    $emailResult  = mysqli_query($sql, $emailQuery)
                      or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($emailResult) == 0) {
      return null;
    }

    $patient      = new Patient($sql);
    $patient->initializeFromResource($emailResult);

    return $patient;
  }

  /**
    *  Creates the object from the patient's phone number in the database.
    *
    *  @param mysqli $sql  The MySQLi connection to the database which holds the patient's data.
    *  @param int $phone   The patient's phone number.
    *
    *  @since 1.0
    *  @return Patient|null  The created patient's object, or null if the phone number is not found.
    *
    *  @see Patient::CreateFrom_ID()       To create the patient from his ID in the database.
    *  @see Patient::CreateFrom_Email()    To create the patient from the email address.
    *  @see Patient::CreateFrom_Name()     To create the patient from his full name.
    *  @see Patient::GetAll()              To get all patients in the database.
    **/
  public static function CreateFrom_Phone($sql, $phone) {
    // Prepare the phone to go into the database
    $phone        = mysqli_escape_string($sql, $phone);

    // Query the SQL
    $phoneQuery   = "SELECT * FROM memos_patients WHERE phone='$phone'";
    $phoneResult  = mysqli_query($sql, $phoneQuery)
                      or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($phoneResult) == 0) {
      return null;
    }

    $patient      = new Patient($sql);
    $patient->initializeFromResource($phoneResult);

    return $patient;
  }

  /**
    *  Creates the object from the patient's full name.
    *
    *  @param mysqli $sql     The MySQLi connection to the database which holds the patient's data.
    *  @param string $name    The patient's full name.<br>
    *                         Must match this format: <b>First(space)Last</b>
    *
    *  @since 1.0
    *  @return Patient|null  The created patient's object, or null if the full name is not found.
    *
    *  @see Patient::CreateFrom_ID()       To create the patient from his ID in the database.
    *  @see Patient::CreateFrom_Phone()    To create the patient from the phone number.
    *  @see Patient::CreateFrom_Email()    To create the patient from the email address.
    *  @see Patient::GetAll()              To get all patients in the database.
    **/
  public static function CreateFrom_Name($sql, $full_name) {
    // Split the name by spaces
    $splitted_name  = explode(' ', $full_name);

    // Get the name parts from the array and prepare them to go into the database
    $first_name     = mysqli_escape_string($sql, $splitted_name[0]);
    $last_name      = mysqli_escape_string($sql, $splitted_name[1]);


    // Query the SQL
    $nameQuery      = "SELECT * FROM memos_patients WHERE name_first='$first_name' AND name_last='$last_name'";
    $nameResult     = mysqli_query($sql, $nameQuery)
                        or die(mysqli_error($sql));

    // Process the result
    if(mysqli_num_rows($nameResult) == 0) {
      return null;
    }

    $patient = new Patient($sql);
    $patient->initializeFromResource($nameResult);

    return $patient;
  }

  /**
    *  Get an array of all Patients.
    *
    *  @param mysqli $sql     The MySQLi connection to the database which holds the patient's data.
    *
    *  @since 1.0
    *  @return Patient[]      The array of patient objects.
    *
    *  @see Patient::CreateFrom_ID()       To create the patient from his ID in the database.
    *  @see Patient::CreateFrom_Phone()    To create the patient from the phone number.
    *  @see Patient::CreateFrom_Email()    To create the patient from the email address.
    *  @see Patient::CreateFrom_Name()     To create the patient from his full name.
    **/
  public static function GetAll($sql) {
    // Query the SQL
    $allQuery   = "SELECT * FROM memos_patients";
    $allResult  = mysqli_query($sql, $allQuery)
                    or die(mysqli_error($sql));

    // Create an array of patients and iterate thru all rows in the database
    $patients = array();
    for($index = 0; $index < mysqli_num_rows($allResult); $index ++) {
      // Initialize each patient and put the patient into the patients array
      $patient = new Patient($sql);
      $patient->initializeFromResource($allResult, $index);

      array_push($patients, $patient);
    }

    return $patients;
  }

  /**
    * Insert a new patient into the database.
    *
    * @param mysqli $sql        The MySQLi connection to the database which holds the patient's data.
    * @param string $name_first The first name of the patient.
    * @param string $name_last  The last name of the patient.
    * @param int    $dob        The unix timestamp of the patient's date of birth.
    * @param string $email      The patient's email address.
    * @param int    $phone      The patient's phone number.
    *
    * @since 1.0
    * @return Patient           The created patient's object.
    */
  public static function InsertNew($sql, $name_first, $name_last, $dob, $email, $phone) {
    // Prepare the values to go into the database
    $name_first   = mysqli_escape_string($sql, $name_first);
    $name_last    = mysqli_escape_string($sql, $name_last);
    $dob          = mysqli_escape_string($sql, $dob);
    $email        = mysqli_escape_string($sql, $email);
    $phone        = mysqli_escape_string($sql, $phone);

    // Query the SQL
    $insertQuery  = "INSERT INTO memos_patients (name_first, name_last, dob, email, phone) VALUES
                    ('$name_first', '$name_last', '$dob', '$email', '$phone')";
    $insertResult = mysqli_query($sql, $insertQuery)
                      or die(mysqli_error($sql));

    // Return the added Patient object
    return Patient::CreateFrom_Name($sql, $name_first . " " . $name_last);
  }



  /**
   *  Set's the MySQLi connection to the database and prepares the Patient class for working.
   *
   *  @param mysqli $sql        The MySQLi connection to the database which holds the patient's data.
   *
   *  @since 1.0
   */
  public function __construct($sql) {
    $this->sql = $sql;
  }

  /**
   *  Initializes this object to contain the patient's data retrieved from given resource and row index.
   *
   *  @param mysqli_result  $resource The MySQLi resource which holds the patient's data.
   *  @param int            $index    The row index in the resource.
   *
   *  @since 1.0
   *  @see mysqli_result  Used function to easily one-line get the field from the resource.
   */
  public function initializeFromResource($resource, $index = 0) {
    $this->ID           = mysqli_result($resource, $index, "ID");
    $this->name_first   = mysqli_result($resource, $index, "name_first");
    $this->name_last    = mysqli_result($resource, $index, "name_last");
    $this->dob          = mysqli_result($resource, $index, "dob");
    $this->email        = mysqli_result($resource, $index, "email");
    $this->phone        = mysqli_result($resource, $index, "phone");
  }

  /**
   *  Delete the patient from the database.
   *
   *  @since 1.0
   */
  public function delete() {
    // Query the SQL
    $deleteQuery  = "DELETE FROM memos_patients WHERE ID='" . $this->ID . "'";
    $deleteResult = mysqli_query($this->sql, $deleteQuery)
                      or die(mysqli_error($this->sql));
  }

  /**
   *  Get the patient's ID in the database.
   *
   *  @since 1.0
   *
   *  @return int The patient's ID in the database.
   */
  public function id() {
    return $this->ID;
  }

  /**
   *  Get the patient's first name, or set a new value to the database.
   *
   *  @param string $newValue The patient's new first name.
   *
   *  @since 1.0
   *
   *  @return string|null Returns first name of the patient if the new value was null,
   *                      or returns null when the request was to set new value.
   */
  public function name_first($newValue = null) {
    if($newValue == null) {
      // The request was to get the first name
      return $this->name_first;
    }

    // The request was to set the first name
    // Prepare the new first name to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_patients SET name_first='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->name_first   = $newValue;
    return null;
  }

  /**
   *  Get the patient's last name, or set a new value to the database.
   *
   *  @param string $newValue The patient's new last name.
   *
   *  @since 1.0
   *
   *  @return string|null Returns last name of the patient if the new value was null,
   *                      or returns null when the request was to set new value.
   */
  public function name_last($newValue = null) {
    if($newValue == null) {
      // The request was to get the last name
      return $this->name_last;
    }

    // The request was to set the last name
    // Prepare the new last name to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_patients SET name_last='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->name_last    = $newValue;
    return null;
  }

  /**
   *  Get the patient's data of birth in unix timestamp, or set a new value to the database.
   *
   *  @param int $newValue The patient's new date of birth.
   *
   *  @since 1.0
   *
   *  @return int|null  Returns unix timestamp of the patient's date of birth if the new value was null,
   *                    or returns null when the request was to set new value.
   */
  public function dob($newValue = null) {
    if($newValue == null) {
      // The request was to get the date of birth
      return $this->dob;
    }

    // The request was to set the date of birth
    // Prepare the new date of birth to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_patients SET dob='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->dob          = $newValue;
    return null;
  }

  /**
   *  Get the patient's email address, or set a new value to the database.
   *
   *  @param string $newValue The patient's new email address.
   *
   *  @since 1.0
   *
   *  @return string|null Returns patient's email address if the new value was null,
   *                      or returns null when the request was to set new value.
   */
  public function email($newValue = null) {
    if($newValue == null) {
      // The request was to get the email
      return $this->email;
    }

    // The request was to set the email
    // Prepare the new email to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_patients SET email='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->email        = $newValue;
    return null;
  }

  /**
   *  Get the patient's phone number, or set a new value to the database.
   *
   *  @param int $newValue The patient's new phone number.
   *
   *  @since 1.0
   *
   *  @return int|null  Returns patient's phone number if the new value was null,
   *                    or returns null when the request was to set new value.
   */
  public function phone($newValue = null) {
    if($newValue == null) {
      // The request was to get the phone
      return $this->phone;
    }

    // The request was to set the phone
    // Prepare the new phone to go into the database
    $newValue           = mysqli_escape_string($this->sql, $newValue);

    // Query the SQL
    $updateQuery        = "UPDATE memos_patients SET phone='$newValue' WHERE ID='" . $this->ID . "'";
    $updateResult       = mysqli_query($this->sql, $updateQuery)
                            or die(mysqli_error($this->sql));

    // Update the new value
    $this->phone        = $newValue;
    return null;
  }
}
?>
