<?php

if(!isset($libs_path)) {
  die("Error: The variable libs_path is not defined.");
}

$sql_username   = "";
$sql_password   = "";
$sql_host       = "";
$sql_database   = "";



$sql = mysqli_connect($sql_host, $sql_username, $sql_password, $sql_database)
  or die(mysqli_error($sql));



// Parsed utility function to get the field from result easily and quick
function mysqli_result($result,$row,$field=0) {
    if ($result===false) return false;
    if ($row>=mysqli_num_rows($result)) return false;
    if (is_string($field) && !(strpos($field,".")===false)) {
        $t_field=explode(".",$field);
        $field=-1;
        $t_fields=mysqli_fetch_fields($result);
        for ($id=0;$id<mysqli_num_fields($result);$id++) {
            if ($t_fields[$id]->table==$t_field[0] && $t_fields[$id]->name==$t_field[1]) {
                $field=$id;
                break;
            }
        }
        if ($field==-1) return false;
    }
    mysqli_data_seek($result,$row);
    $line=mysqli_fetch_array($result);
    return isset($line[$field]) ? $line[$field] : false;
}

require_once $libs_path . "Memos/Patient.php";
require_once $libs_path . "Memos/Diagnose.php";
require_once $libs_path . "Memos/Found_Diagnose.php";

?>
